var book_items = [];
book_items[2] = [
	[1, "Том 1, ч.1"],
	[124, "Том 1, ч.2"],
	[223, "Том 1, ч.3"],
	[336, "Том 2, ч.1"],
	[403, "Том 2, ч.2"],
	[489, "Том 2, ч.3"],
	[581, "Том 2, ч.4"],
	[640, "Том 2, ч.5"],
	[722, "Том 3, ч.1"],
	[825, "Том 3, ч.2"],
	[994, "Том 3, ч.3"],
	[1126, "Том 4, ч.1"],
	[1188, "Том 4, ч.2"],
	[1240, "Том 4, ч.3"],
	[1295, "Том 4, ч.4"],
	[1361, "Эпилог, ч.1"],
	[1425, "Эпилог, ч.2"]];

book_items[3] = [
	[1474, "Гл. 1"],
	[1484, "Гл. 2"],
	[1498, "Гл. 3"],
	[1513, "Гл. 4"],
	[1531, "Гл. 5"],
	[1547, "Гл. 6"],
	[1563, "Гл. 7"],
	[1580, "Гл. 8"],
	[1600, "Гл. 9"],
	[1615, "Гл. 10"],
	[1629, "Гл. 11"]];

book_items[4] = [
	[1656, "От авторов"],
	[1657, "I. О том, как Паниковский нарушил конвенцию"],
	[1664, "II. Тридцать сыновей лейтенанта Шмидта"],
	[1673, "III. Бензин ваш - идеи наши"],
	[1679, "IV. Обыкновенный чемоданишко"],
	[1684, "V. Подземное царство"],
	[1688, "VI. \"Антилопа - Гну\""],
	[1694, "VII. Сладкое бремя славы"],
	[1700, "VIII. Кризис жанра"],
	[1707, "IX. Снова кризис жанра"],
	[1713, "X. Телеграмма от братьев Карамазовых"],
	[1715, "XI. Геркулесовцы"],
	[1720, "XII. Гомер, Мильтон и Паниковский"],
	[1725, "XIII. Васисуалий Лоханкин и его роль в русской революции"],
	[1734, "XIV. Первое свидание"],
	[1741, "XV. Рога и копыта"],
	[1748, "XVI. Ярбух фюр психоаналитик"],
	[1754, "XVII. Блудный сын возвращается домой"],
	[1759, "XVIII. На суше и на море"],
	[1766, "XIX. Универсальный штемпель"],
	[1769, "XX. Командор танцует танго"],
	[1775, "XXI. Конец \"Вороньей слободки\""],
	[1779, "XXII. Командовать парадом буду я"],
	[1784, "XXIII. Сердце шофера"],
	[1790, "XXIV. Погода благоприятствовала любви"],
	[1796, "XXV. Три дороги"],
	[1802, "XXVI. Пассажир литерного поезда"],
	[1807, "XXVII. \"Позвольте войти наемнику капитала\""],
	[1812, "XXVIII. Потный вал вдохновенья"],
	[1817, "XXIX. Гремящий Ключ"],
	[1823, "XXX. Александр Ибн-Иванович"],
	[1828, "XXXI. Багдад"],
	[1831, "XXXII. Врата великих возможностей"],
	[1835, "XXXIII. Индийский гость"],
	[1838, "XXXIV. Дружба с юностью"],
	[1844, "XXXV. Его любили домашние хозяйки, домашние работницы, вдовы и даже одна женщина - зубной техник"],
	[1850, "XXXVI. Кавалер ордена Золотого Руна"]];

book_items[1] = [
	[1854,	"Полноте, ребята"],
	[1855,	"Светит месяц"],
	[1856,	"Заиграй, моя волынка"],
	[1857,	"Волга реченька, глубока"],
	[1858,	"Кумушки"],
	[1859,	"Ах ты, береза"],
	[1860,	"И.С. Бах Аллегро"],
	[1861,	"И.С. Бах. Скерцо"],
	[1862,	"В.В. Андреев. Мазурка"],
	[1863,	"Ах ты, вечер"],
	[1864,	"Из-за острова"]];

var playlist_names = [
	[1, "&quot;Война и мир&quot; том 1, ч.1, гл.1"],
	[5, "&quot;Война и мир&quot; том 1, ч.1, гл.2"],
	[8, "&quot;Война и мир&quot; том 1, ч.1, гл.3"],
	[12, "&quot;Война и мир&quot; том 1, ч.1, гл.4"],
	[19, "&quot;Война и мир&quot; том 1, ч.1, гл.5"],
	[23, "&quot;Война и мир&quot; том 1, ч.1, гл.6"],
	[35, "&quot;Война и мир&quot; том 1, ч.1, гл.7"],
	[39, "&quot;Война и мир&quot; том 1, ч.1, гл.8"],
	[41, "&quot;Война и мир&quot; том 1, ч.1, гл.9"],
	[44, "&quot;Война и мир&quot; том 1, ч.1, гл.10"],
	[46, "&quot;Война и мир&quot; том 1, ч.1, гл.11"],
	[50, "&quot;Война и мир&quot; том 1, ч.1, гл.12"],
	[54, "&quot;Война и мир&quot; том 1, ч.1, гл.13"],
	[60, "&quot;Война и мир&quot; том 1, ч.1, гл.14"],
	[62, "&quot;Война и мир&quot; том 1, ч.1, гл.15"],
	[68, "&quot;Война и мир&quot; том 1, ч.1, гл.16"],
	[71, "&quot;Война и мир&quot; том 1, ч.1, гл.17"],
	[76, "&quot;Война и мир&quot; том 1, ч.1, гл.18"],
	[83, "&quot;Война и мир&quot; том 1, ч.1, гл.19"],
	[88, "&quot;Война и мир&quot; том 1, ч.1, гл.20"],
	[93, "&quot;Война и мир&quot; том 1, ч.1, гл.21"],
	[97, "&quot;Война и мир&quot; том 1, ч.1, гл.22"],
	[107, "&quot;Война и мир&quot; том 1, ч.1, гл.23"],
	[112, "&quot;Война и мир&quot; том 1, ч.1, гл.24"],
	[116, "&quot;Война и мир&quot; том 1, ч.1, гл.25"],
	[124, "&quot;Война и мир&quot; том 1, ч.2, гл.1"],
	[128, "&quot;Война и мир&quot; том 1, ч.2, гл.2"],
	[134, "&quot;Война и мир&quot; том 1, ч.2, гл.3"],
	[140, "&quot;Война и мир&quot; том 1, ч.2, гл.4"],
	[147, "&quot;Война и мир&quot; том 1, ч.2, гл.5"],
	[150, "&quot;Война и мир&quot; том 1, ч.2, гл.6"],
	[152, "&quot;Война и мир&quot; том 1, ч.2, гл.7"],
	[157, "&quot;Война и мир&quot; том 1, ч.2, гл.8"],
	[166, "&quot;Война и мир&quot; том 1, ч.2, гл.9"],
	[170, "&quot;Война и мир&quot; том 1, ч.2, гл.10"],
	[176, "&quot;Война и мир&quot; том 1, ч.2, гл.11"],
	[178, "&quot;Война и мир&quot; том 1, ч.2, гл.12"],
	[183, "&quot;Война и мир&quot; том 1, ч.2, гл.13"],
	[188, "&quot;Война и мир&quot; том 1, ч.2, гл.14"],
	[191, "&quot;Война и мир&quot; том 1, ч.2, гл.15"],
	[196, "&quot;Война и мир&quot; том 1, ч.2, гл.16"],
	[198, "&quot;Война и мир&quot; том 1, ч.2, гл.17"],
	[203, "&quot;Война и мир&quot; том 1, ч.2, гл.18"],
	[207, "&quot;Война и мир&quot; том 1, ч.2, гл.19"],
	[212, "&quot;Война и мир&quot; том 1, ч.2, гл.20"],
	[217, "&quot;Война и мир&quot; том 1, ч.2, гл.21"],
	[223, "&quot;Война и мир&quot; том 1, ч.3, гл.1"],
	[231, "&quot;Война и мир&quot; том 1, ч.3, гл.2"],
	[239, "&quot;Война и мир&quot; том 1, ч.3, гл.3"],
	[248, "&quot;Война и мир&quot; том 1, ч.3, гл.4"],
	[256, "&quot;Война и мир&quot; том 1, ч.3, гл.5"],
	[262, "&quot;Война и мир&quot; том 1, ч.3, гл.6"],
	[267, "&quot;Война и мир&quot; том 1, ч.3, гл.7"],
	[274, "&quot;Война и мир&quot; том 1, ч.3, гл.8"],
	[279, "&quot;Война и мир&quot; том 1, ч.3, гл.9"],
	[285, "&quot;Война и мир&quot; том 1, ч.3, гл.10"],
	[290, "&quot;Война и мир&quot; том 1, ч.3, гл.11"],
	[294, "&quot;Война и мир&quot; том 1, ч.3, гл.12"],
	[300, "&quot;Война и мир&quot; том 1, ч.3, гл.13"],
	[305, "&quot;Война и мир&quot; том 1, ч.3, гл.14"],
	[309, "&quot;Война и мир&quot; том 1, ч.3, гл.15"],
	[314, "&quot;Война и мир&quot; том 1, ч.3, гл.16"],
	[318, "&quot;Война и мир&quot; том 1, ч.3, гл.17"],
	[323, "&quot;Война и мир&quot; том 1, ч.3, гл.18"],
	[330, "&quot;Война и мир&quot; том 1, ч.3, гл.19"],
	[336, "&quot;Война и мир&quot; том 2, ч.1, гл.1"],
	[344, "&quot;Война и мир&quot; том 2, ч.1, гл.2"],
	[351, "&quot;Война и мир&quot; том 2, ч.1, гл.3"],
	[356, "&quot;Война и мир&quot; том 2, ч.1, гл.4"],
	[361, "&quot;Война и мир&quot; том 2, ч.1, гл.5"],
	[363, "&quot;Война и мир&quot; том 2, ч.1, гл.6"],
	[368, "&quot;Война и мир&quot; том 2, ч.1, гл.7"],
	[372, "&quot;Война и мир&quot; том 2, ч.1, гл.8"],
	[377, "&quot;Война и мир&quot; том 2, ч.1, гл.9"],
	[380, "&quot;Война и мир&quot; том 2, ч.1, гл.10"],
	[384, "&quot;Война и мир&quot; том 2, ч.1, гл.11"],
	[387, "&quot;Война и мир&quot; том 2, ч.1, гл.12"],
	[390, "&quot;Война и мир&quot; том 2, ч.1, гл.13"],
	[393, "&quot;Война и мир&quot; том 2, ч.1, гл.14"],
	[397, "&quot;Война и мир&quot; том 2, ч.1, гл.15"],
	[400, "&quot;Война и мир&quot; том 2, ч.1, гл.16"],
	[403, "&quot;Война и мир&quot; том 2, ч.2, гл.1"],
	[407, "&quot;Война и мир&quot; том 2, ч.2, гл.2"],
	[414, "&quot;Война и мир&quot; том 2, ч.2, гл.3"],
	[422, "&quot;Война и мир&quot; том 2, ч.2, гл.4"],
	[426, "&quot;Война и мир&quot; том 2, ч.2, гл.5"],
	[428, "&quot;Война и мир&quot; том 2, ч.2, гл.6"],
	[432, "&quot;Война и мир&quot; том 2, ч.2, гл.7"],
	[434, "&quot;Война и мир&quot; том 2, ч.2, гл.8"],
	[438, "&quot;Война и мир&quot; том 2, ч.2, гл.9"],
	[443, "&quot;Война и мир&quot; том 2, ч.2, гл.10"],
	[447, "&quot;Война и мир&quot; том 2, ч.2, гл.11"],
	[454, "&quot;Война и мир&quot; том 2, ч.2, гл.12"],
	[458, "&quot;Война и мир&quot; том 2, ч.2, гл.13"],
	[462, "&quot;Война и мир&quot; том 2, ч.2, гл.14"],
	[464, "&quot;Война и мир&quot; том 2, ч.2, гл.15"],
	[468, "&quot;Война и мир&quot; том 2, ч.2, гл.16"],
	[472, "&quot;Война и мир&quot; том 2, ч.2, гл.17"],
	[475, "&quot;Война и мир&quot; том 2, ч.2, гл.18"],
	[478, "&quot;Война и мир&quot; том 2, ч.2, гл.19"],
	[481, "&quot;Война и мир&quot; том 2, ч.2, гл.20"],
	[484, "&quot;Война и мир&quot; том 2, ч.2, гл.21"],
	[489, "&quot;Война и мир&quot; том 2, ч.3, гл.1"],
	[492, "&quot;Война и мир&quot; том 2, ч.3, гл.2"],
	[495, "&quot;Война и мир&quot; том 2, ч.3, гл.3"],
	[497, "&quot;Война и мир&quot; том 2, ч.3, гл.4"],
	[500, "&quot;Война и мир&quot; том 2, ч.3, гл.5"],
	[505, "&quot;Война и мир&quot; том 2, ч.3, гл.6"],
	[508, "&quot;Война и мир&quot; том 2, ч.3, гл.7"],
	[513, "&quot;Война и мир&quot; том 2, ч.3, гл.8"],
	[516, "&quot;Война и мир&quot; том 2, ч.3, гл.9"],
	[518, "&quot;Война и мир&quot; том 2, ч.3, гл.10"],
	[525, "&quot;Война и мир&quot; том 2, ч.3, гл.11"],
	[528, "&quot;Война и мир&quot; том 2, ч.3, гл.12"],
	[531, "&quot;Война и мир&quot; том 2, ч.3, гл.13"],
	[535, "&quot;Война и мир&quot; том 2, ч.3, гл.14"],
	[539, "&quot;Война и мир&quot; том 2, ч.3, гл.15"],
	[542, "&quot;Война и мир&quot; том 2, ч.3, гл.16"],
	[546, "&quot;Война и мир&quot; том 2, ч.3, гл.17"],
	[548, "&quot;Война и мир&quot; том 2, ч.3, гл.18"],
	[553, "&quot;Война и мир&quot; том 2, ч.3, гл.19"],
	[555, "&quot;Война и мир&quot; том 2, ч.3, гл.20"],
	[558, "&quot;Война и мир&quot; том 2, ч.3, гл.21"],
	[561, "&quot;Война и мир&quot; том 2, ч.3, гл.22"],
	[565, "&quot;Война и мир&quot; том 2, ч.3, гл.23"],
	[571, "&quot;Война и мир&quot; том 2, ч.3, гл.24"],
	[574, "&quot;Война и мир&quot; том 2, ч.3, гл.25"],
	[577, "&quot;Война и мир&quot; том 2, ч.3, гл.26"],
	[581, "&quot;Война и мир&quot; том 2, ч.4, гл.1"],
	[585, "&quot;Война и мир&quot; том 2, ч.4, гл.2"],
	[587, "&quot;Война и мир&quot; том 2, ч.4, гл.3"],
	[590, "&quot;Война и мир&quot; том 2, ч.4, гл.4"],
	[595, "&quot;Война и мир&quot; том 2, ч.4, гл.5"],
	[599, "&quot;Война и мир&quot; том 2, ч.4, гл.6"],
	[605, "&quot;Война и мир&quot; том 2, ч.4, гл.7"],
	[614, "&quot;Война и мир&quot; том 2, ч.4, гл.8"],
	[618, "&quot;Война и мир&quot; том 2, ч.4, гл.9"],
	[622, "&quot;Война и мир&quot; том 2, ч.4, гл.10"],
	[630, "&quot;Война и мир&quot; том 2, ч.4, гл.11"],
	[634, "&quot;Война и мир&quot; том 2, ч.4, гл.12"],
	[637, "&quot;Война и мир&quot; том 2, ч.4, гл.13"],
	[640, "&quot;Война и мир&quot; том 2, ч.5, гл.1"],
	[645, "&quot;Война и мир&quot; том 2, ч.5, гл.2"],
	[649, "&quot;Война и мир&quot; том 2, ч.5, гл.3"],
	[655, "&quot;Война и мир&quot; том 2, ч.5, гл.4"],
	[657, "&quot;Война и мир&quot; том 2, ч.5, гл.5"],
	[661, "&quot;Война и мир&quot; том 2, ч.5, гл.6"],
	[664, "&quot;Война и мир&quot; том 2, ч.5, гл.7"],
	[668, "&quot;Война и мир&quot; том 2, ч.5, гл.8"],
	[673, "&quot;Война и мир&quot; том 2, ч.5, гл.9"],
	[678, "&quot;Война и мир&quot; том 2, ч.5, гл.10"],
	[681, "&quot;Война и мир&quot; том 2, ч.5, гл.11"],
	[684, "&quot;Война и мир&quot; том 2, ч.5, гл.12"],
	[687, "&quot;Война и мир&quot; том 2, ч.5, гл.13"],
	[690, "&quot;Война и мир&quot; том 2, ч.5, гл.14"],
	[693, "&quot;Война и мир&quot; том 2, ч.5, гл.15"],
	[698, "&quot;Война и мир&quot; том 2, ч.5, гл.16"],
	[702, "&quot;Война и мир&quot; том 2, ч.5, гл.17"],
	[705, "&quot;Война и мир&quot; том 2, ч.5, гл.18"],
	[708, "&quot;Война и мир&quot; том 2, ч.5, гл.19"],
	[712, "&quot;Война и мир&quot; том 2, ч.5, гл.20"],
	[715, "&quot;Война и мир&quot; том 2, ч.5, гл.21"],
	[719, "&quot;Война и мир&quot; том 2, ч.5, гл.22"],
	[722, "&quot;Война и мир&quot; том 3, ч.1, гл.1"],
	[728, "&quot;Война и мир&quot; том 3, ч.1, гл.2"],
	[732, "&quot;Война и мир&quot; том 3, ч.1, гл.3"],
	[736, "&quot;Война и мир&quot; том 3, ч.1, гл.4"],
	[740, "&quot;Война и мир&quot; том 3, ч.1, гл.5"],
	[743, "&quot;Война и мир&quot; том 3, ч.1, гл.6"],
	[751, "&quot;Война и мир&quot; том 3, ч.1, гл.7"],
	[755, "&quot;Война и мир&quot; том 3, ч.1, гл.8"],
	[761, "&quot;Война и мир&quot; том 3, ч.1, гл.9"],
	[768, "&quot;Война и мир&quot; том 3, ч.1, гл.10"],
	[772, "&quot;Война и мир&quot; том 3, ч.1, гл.11"],
	[778, "&quot;Война и мир&quot; том 3, ч.1, гл.12"],
	[781, "&quot;Война и мир&quot; том 3, ч.1, гл.13"],
	[784, "&quot;Война и мир&quot; том 3, ч.1, гл.14"],
	[788, "&quot;Война и мир&quot; том 3, ч.1, гл.15"],
	[791, "&quot;Война и мир&quot; том 3, ч.1, гл.16"],
	[794, "&quot;Война и мир&quot; том 3, ч.1, гл.17"],
	[798, "&quot;Война и мир&quot; том 3, ч.1, гл.18"],
	[804, "&quot;Война и мир&quot; том 3, ч.1, гл.19"],
	[808, "&quot;Война и мир&quot; том 3, ч.1, гл.20"],
	[813, "&quot;Война и мир&quot; том 3, ч.1, гл.21"],
	[818, "&quot;Война и мир&quot; том 3, ч.1, гл.22"],
	[823, "&quot;Война и мир&quot; том 3, ч.1, гл.23"],
	[825, "&quot;Война и мир&quot; том 3, ч.2, гл.1"],
	[830, "&quot;Война и мир&quot; том 3, ч.2, гл.2"],
	[833, "&quot;Война и мир&quot; том 3, ч.2, гл.3"],
	[836, "&quot;Война и мир&quot; том 3, ч.2, гл.4"],
	[846, "&quot;Война и мир&quot; том 3, ч.2, гл.5"],
	[853, "&quot;Война и мир&quot; том 3, ч.2, гл.6"],
	[858, "&quot;Война и мир&quot; том 3, ч.2, гл.7"],
	[862, "&quot;Война и мир&quot; том 3, ч.2, гл.8"],
	[871, "&quot;Война и мир&quot; том 3, ч.2, гл.9"],
	[876, "&quot;Война и мир&quot; том 3, ч.2, гл.10"],
	[880, "&quot;Война и мир&quot; том 3, ч.2, гл.11"],
	[883, "&quot;Война и мир&quot; том 3, ч.2, гл.12"],
	[885, "&quot;Война и мир&quot; том 3, ч.2, гл.13"],
	[888, "&quot;Война и мир&quot; том 3, ч.2, гл.14"],
	[893, "&quot;Война и мир&quot; том 3, ч.2, гл.15"],
	[898, "&quot;Война и мир&quot; том 3, ч.2, гл.16"],
	[902, "&quot;Война и мир&quot; том 3, ч.2, гл.17"],
	[907, "&quot;Война и мир&quot; том 3, ч.2, гл.18"],
	[912, "&quot;Война и мир&quot; том 3, ч.2, гл.19"],
	[917, "&quot;Война и мир&quot; том 3, ч.2, гл.20"],
	[921, "&quot;Война и мир&quot; том 3, ч.2, гл.21"],
	[926, "&quot;Война и мир&quot; том 3, ч.2, гл.22"],
	[929, "&quot;Война и мир&quot; том 3, ч.2, гл.23"],
	[931, "&quot;Война и мир&quot; том 3, ч.2, гл.24"],
	[934, "&quot;Война и мир&quot; том 3, ч.2, гл.25"],
	[941, "&quot;Война и мир&quot; том 3, ч.2, гл.26"],
	[946, "&quot;Война и мир&quot; том 3, ч.2, гл.27"],
	[949, "&quot;Война и мир&quot; том 3, ч.2, гл.28"],
	[952, "&quot;Война и мир&quot; том 3, ч.2, гл.29"],
	[955, "&quot;Война и мир&quot; том 3, ч.2, гл.30"],
	[958, "&quot;Война и мир&quot; том 3, ч.2, гл.31"],
	[965, "&quot;Война и мир&quot; том 3, ч.2, гл.32"],
	[967, "&quot;Война и мир&quot; том 3, ч.2, гл.33"],
	[970, "&quot;Война и мир&quot; том 3, ч.2, гл.34"],
	[975, "&quot;Война и мир&quot; том 3, ч.2, гл.35"],
	[978, "&quot;Война и мир&quot; том 3, ч.2, гл.36"],
	[983, "&quot;Война и мир&quot; том 3, ч.2, гл.37"],
	[987, "&quot;Война и мир&quot; том 3, ч.2, гл.38"],
	[991, "&quot;Война и мир&quot; том 3, ч.2, гл.39"],
	[994, "&quot;Война и мир&quot; том 3, ч.3, гл.1"],
	[998, "&quot;Война и мир&quot; том 3, ч.3, гл.2"],
	[1002, "&quot;Война и мир&quot; том 3, ч.3, гл.3"],
	[1005, "&quot;Война и мир&quot; том 3, ч.3, гл.4"],
	[1009, "&quot;Война и мир&quot; том 3, ч.3, гл.5"],
	[1012, "&quot;Война и мир&quot; том 3, ч.3, гл.6"],
	[1015, "&quot;Война и мир&quot; том 3, ч.3, гл.7"],
	[1018, "&quot;Война и мир&quot; том 3, ч.3, гл.8"],
	[1020, "&quot;Война и мир&quot; том 3, ч.3, гл.9"],
	[1023, "&quot;Война и мир&quot; том 3, ч.3, гл.10"],
	[1026, "&quot;Война и мир&quot; том 3, ч.3, гл.11"],
	[1029, "&quot;Война и мир&quot; том 3, ч.3, гл.12"],
	[1033, "&quot;Война и мир&quot; том 3, ч.3, гл.13"],
	[1037, "&quot;Война и мир&quot; том 3, ч.3, гл.14"],
	[1039, "&quot;Война и мир&quot; том 3, ч.3, гл.15"],
	[1042, "&quot;Война и мир&quot; том 3, ч.3, гл.16"],
	[1046, "&quot;Война и мир&quot; том 3, ч.3, гл.17"],
	[1050, "&quot;Война и мир&quot; том 3, ч.3, гл.18"],
	[1053, "&quot;Война и мир&quot; том 3, ч.3, гл.19"],
	[1058, "&quot;Война и мир&quot; том 3, ч.3, гл.20"],
	[1060, "&quot;Война и мир&quot; том 3, ч.3, гл.21"],
	[1062, "&quot;Война и мир&quot; том 3, ч.3, гл.22"],
	[1065, "&quot;Война и мир&quot; том 3, ч.3, гл.23"],
	[1069, "&quot;Война и мир&quot; том 3, ч.3, гл.24"],
	[1073, "&quot;Война и мир&quot; том 3, ч.3, гл.25"],
	[1082, "&quot;Война и мир&quot; том 3, ч.3, гл.26"],
	[1088, "&quot;Война и мир&quot; том 3, ч.3, гл.27"],
	[1091, "&quot;Война и мир&quot; том 3, ч.3, гл.28"],
	[1094, "&quot;Война и мир&quot; том 3, ч.3, гл.29"],
	[1104, "&quot;Война и мир&quot; том 3, ч.3, гл.30"],
	[1106, "&quot;Война и мир&quot; том 3, ч.3, гл.31"],
	[1110, "&quot;Война и мир&quot; том 3, ч.3, гл.32"],
	[1117, "&quot;Война и мир&quot; том 3, ч.3, гл.33"],
	[1122, "&quot;Война и мир&quot; том 3, ч.3, гл.34"],
	[1126, "&quot;Война и мир&quot; том 4, ч.1, гл.1"],
	[1130, "&quot;Война и мир&quot; том 4, ч.1, гл.2"],
	[1133, "&quot;Война и мир&quot; том 4, ч.1, гл.3"],
	[1136, "&quot;Война и мир&quot; том 4, ч.1, гл.4"],
	[1141, "&quot;Война и мир&quot; том 4, ч.1, гл.5"],
	[1144, "&quot;Война и мир&quot; том 4, ч.1, гл.6"],
	[1148, "&quot;Война и мир&quot; том 4, ч.1, гл.7"],
	[1152, "&quot;Война и мир&quot; том 4, ч.1, гл.8"],
	[1156, "&quot;Война и мир&quot; том 4, ч.1, гл.9"],
	[1158, "&quot;Война и мир&quot; том 4, ч.1, гл.10"],
	[1161, "&quot;Война и мир&quot; том 4, ч.1, гл.11"],
	[1165, "&quot;Война и мир&quot; том 4, ч.1, гл.12"],
	[1169, "&quot;Война и мир&quot; том 4, ч.1, гл.13"],
	[1173, "&quot;Война и мир&quot; том 4, ч.1, гл.14"],
	[1178, "&quot;Война и мир&quot; том 4, ч.1, гл.15"],
	[1182, "&quot;Война и мир&quot; том 4, ч.1, гл.16"],
	[1188, "&quot;Война и мир&quot; том 4, ч.2, гл.1"],
	[1191, "&quot;Война и мир&quot; том 4, ч.2, гл.2"],
	[1193, "&quot;Война и мир&quot; том 4, ч.2, гл.3"],
	[1196, "&quot;Война и мир&quot; том 4, ч.2, гл.4"],
	[1198, "&quot;Война и мир&quot; том 4, ч.2, гл.5"],
	[1200, "&quot;Война и мир&quot; том 4, ч.2, гл.6"],
	[1203, "&quot;Война и мир&quot; том 4, ч.2, гл.7"],
	[1205, "&quot;Война и мир&quot; том 4, ч.2, гл.8"],
	[1207, "&quot;Война и мир&quot; том 4, ч.2, гл.9"],
	[1210, "&quot;Война и мир&quot; том 4, ч.2, гл.10"],
	[1214, "&quot;Война и мир&quot; том 4, ч.2, гл.11"],
	[1219, "&quot;Война и мир&quot; том 4, ч.2, гл.12"],
	[1222, "&quot;Война и мир&quot; том 4, ч.2, гл.13"],
	[1225, "&quot;Война и мир&quot; том 4, ч.2, гл.14"],
	[1229, "&quot;Война и мир&quot; том 4, ч.2, гл.15"],
	[1231, "&quot;Война и мир&quot; том 4, ч.2, гл.16"],
	[1233, "&quot;Война и мир&quot; том 4, ч.2, гл.17"],
	[1236, "&quot;Война и мир&quot; том 4, ч.2, гл.18"],
	[1238, "&quot;Война и мир&quot; том 4, ч.2, гл.19"],
	[1240, "&quot;Война и мир&quot; том 4, ч.3, гл.1"],
	[1243, "&quot;Война и мир&quot; том 4, ч.3, гл.2"],
	[1245, "&quot;Война и мир&quot; том 4, ч.3, гл.3"],
	[1248, "&quot;Война и мир&quot; том 4, ч.3, гл.4"],
	[1252, "&quot;Война и мир&quot; том 4, ч.3, гл.5"],
	[1256, "&quot;Война и мир&quot; том 4, ч.3, гл.6"],
	[1258, "&quot;Война и мир&quot; том 4, ч.3, гл.7"],
	[1261, "&quot;Война и мир&quot; том 4, ч.3, гл.8"],
	[1263, "&quot;Война и мир&quot; том 4, ч.3, гл.9"],
	[1266, "&quot;Война и мир&quot; том 4, ч.3, гл.10"],
	[1270, "&quot;Война и мир&quot; том 4, ч.3, гл.11"],
	[1273, "&quot;Война и мир&quot; том 4, ч.3, гл.12"],
	[1276, "&quot;Война и мир&quot; том 4, ч.3, гл.13"],
	[1279, "&quot;Война и мир&quot; том 4, ч.3, гл.14"],
	[1281, "&quot;Война и мир&quot; том 4, ч.3, гл.15"],
	[1284, "&quot;Война и мир&quot; том 4, ч.3, гл.16"],
	[1286, "&quot;Война и мир&quot; том 4, ч.3, гл.17"],
	[1288, "&quot;Война и мир&quot; том 4, ч.3, гл.18"],
	[1290, "&quot;Война и мир&quot; том 4, ч.3, гл.19"],
	[1295, "&quot;Война и мир&quot; том 4, ч.4, гл.1"],
	[1299, "&quot;Война и мир&quot; том 4, ч.4, гл.2"],
	[1301, "&quot;Война и мир&quot; том 4, ч.4, гл.3"],
	[1304, "&quot;Война и мир&quot; том 4, ч.4, гл.4"],
	[1307, "&quot;Война и мир&quot; том 4, ч.4, гл.5"],
	[1311, "&quot;Война и мир&quot; том 4, ч.4, гл.6"],
	[1314, "&quot;Война и мир&quot; том 4, ч.4, гл.7"],
	[1316, "&quot;Война и мир&quot; том 4, ч.4, гл.8"],
	[1320, "&quot;Война и мир&quot; том 4, ч.4, гл.9"],
	[1322, "&quot;Война и мир&quot; том 4, ч.4, гл.10"],
	[1327, "&quot;Война и мир&quot; том 4, ч.4, гл.11"],
	[1329, "&quot;Война и мир&quot; том 4, ч.4, гл.12"],
	[1332, "&quot;Война и мир&quot; том 4, ч.4, гл.13"],
	[1337, "&quot;Война и мир&quot; том 4, ч.4, гл.14"],
	[1339, "&quot;Война и мир&quot; том 4, ч.4, гл.15"],
	[1342, "&quot;Война и мир&quot; том 4, ч.4, гл.16"],
	[1345, "&quot;Война и мир&quot; том 4, ч.4, гл.17"],
	[1351, "&quot;Война и мир&quot; том 4, ч.4, гл.18"],
	[1357, "&quot;Война и мир&quot; том 4, ч.4, гл.19"],
	[1359, "&quot;Война и мир&quot; том 4, ч.4, гл.20"],
	[1361, "&quot;Война и мир&quot; Эпилог, ч.1, гл.1"],
	[1365, "&quot;Война и мир&quot; Эпилог, ч.1, гл.2"],
	[1367, "&quot;Война и мир&quot; Эпилог, ч.1, гл.3"],
	[1371, "&quot;Война и мир&quot; Эпилог, ч.1, гл.4"],
	[1374, "&quot;Война и мир&quot; Эпилог, ч.1, гл.5"],
	[1377, "&quot;Война и мир&quot; Эпилог, ч.1, гл.6"],
	[1382, "&quot;Война и мир&quot; Эпилог, ч.1, гл.7"],
	[1385, "&quot;Война и мир&quot; Эпилог, ч.1, гл.8"],
	[1389, "&quot;Война и мир&quot; Эпилог, ч.1, гл.9"],
	[1394, "&quot;Война и мир&quot; Эпилог, ч.1, гл.10"],
	[1399, "&quot;Война и мир&quot; Эпилог, ч.1, гл.11"],
	[1402, "&quot;Война и мир&quot; Эпилог, ч.1, гл.12"],
	[1406, "&quot;Война и мир&quot; Эпилог, ч.1, гл.13"],
	[1409, "&quot;Война и мир&quot; Эпилог, ч.1, гл.14"],
	[1415, "&quot;Война и мир&quot; Эпилог, ч.1, гл.15"],
	[1419, "&quot;Война и мир&quot; Эпилог, ч.1, гл.16"],
	[1425, "&quot;Война и мир&quot; Эпилог, ч.2, гл.1"],
	[1430, "&quot;Война и мир&quot; Эпилог, ч.2, гл.2"],
	[1435, "&quot;Война и мир&quot; Эпилог, ч.2, гл.3"],
	[1437, "&quot;Война и мир&quot; Эпилог, ч.2, гл.4"],
	[1443, "&quot;Война и мир&quot; Эпилог, ч.2, гл.5"],
	[1446, "&quot;Война и мир&quot; Эпилог, ч.2, гл.6"],
	[1450, "&quot;Война и мир&quot; Эпилог, ч.2, гл.7"],
	[1455, "&quot;Война и мир&quot; Эпилог, ч.2, гл.8"],
	[1459, "&quot;Война и мир&quot; Эпилог, ч.2, гл.9"],
	[1464, "&quot;Война и мир&quot; Эпилог, ч.2, гл.10"],
	[1470, "&quot;Война и мир&quot; Эпилог, ч.2, гл.11"],
	[1472, "&quot;Война и мир&quot; Эпилог, ч.2, гл.12"],

	[1474, "&quot;Мёртвые души&quot; гл. 1"],
	[1484, "&quot;Мёртвые души&quot; гл. 2"],
	[1498, "&quot;Мёртвые души&quot; гл. 3"],
	[1513, "&quot;Мёртвые души&quot; гл. 4"],
	[1531, "&quot;Мёртвые души&quot; гл. 5"],
	[1547, "&quot;Мёртвые души&quot; гл. 6"],
	[1563, "&quot;Мёртвые души&quot; гл. 7"],
	[1580, "&quot;Мёртвые души&quot; гл. 8"],
	[1600, "&quot;Мёртвые души&quot; гл. 9"],
	[1615, "&quot;Мёртвые души&quot; гл. 10"],
	[1629, "&quot;Мёртвые души&quot; гл. 11"],

	[1656, "&quot;Золотой теленок&quot; От авторов"],
	[1657, "&quot;Золотой теленок&quot; I. О том, как Паниковский нарушил конвенцию"],
	[1664, "&quot;Золотой теленок&quot; II. Тридцать сыновей лейтенанта Шмидта"],
	[1673, "&quot;Золотой теленок&quot; III. Бензин ваш - идеи наши"],
	[1679, "&quot;Золотой теленок&quot; IV. Обыкновенный чемоданишко"],
	[1684, "&quot;Золотой теленок&quot; V. Подземное царство"],
	[1688, "&quot;Золотой теленок&quot; VI. &quot;Антилопа - Гну&quot;"],
	[1694, "&quot;Золотой теленок&quot; VII. Сладкое бремя славы"],
	[1700, "&quot;Золотой теленок&quot; VIII. Кризис жанра"],
	[1707, "&quot;Золотой теленок&quot; IX. Снова кризис жанра"],
	[1713, "&quot;Золотой теленок&quot; X. Телеграмма от братьев Карамазовых"],
	[1715, "&quot;Золотой теленок&quot; XI. Геркулесовцы"],
	[1720, "&quot;Золотой теленок&quot; XII. Гомер, Мильтон и Паниковский"],
	[1725, "&quot;Золотой теленок&quot; XIII. Васисуалий Лоханкин и его роль в русской революции"],
	[1734, "&quot;Золотой теленок&quot; XIV. Первое свидание"],
	[1741, "&quot;Золотой теленок&quot; XV. Рога и копыта"],
	[1748, "&quot;Золотой теленок&quot; XVI. Ярбух фюр психоаналитик"],
	[1754, "&quot;Золотой теленок&quot; XVII. Блудный сын возвращается домой"],
	[1759, "&quot;Золотой теленок&quot; XVIII. На суше и на море"],
	[1766, "&quot;Золотой теленок&quot; XIX. Универсальный штемпель"],
	[1769, "&quot;Золотой теленок&quot; XX. Командор танцует танго"],
	[1775, "&quot;Золотой теленок&quot; XXI. Конец &quot;Вороньей слободки&quot;"],
	[1779, "&quot;Золотой теленок&quot; XXII. Командовать парадом буду я"],
	[1784, "&quot;Золотой теленок&quot; XXIII. Сердце шофера"],
	[1790, "&quot;Золотой теленок&quot; XXIV. Погода благоприятствовала любви"],
	[1796, "&quot;Золотой теленок&quot; XXV. Три дороги"],
	[1802, "&quot;Золотой теленок&quot; XXVI. Пассажир литерного поезда"],
	[1807, "&quot;Золотой теленок&quot; XXVII. &quot;Позвольте войти наемнику капитала&quot;"],
	[1812, "&quot;Золотой теленок&quot; XXVIII. Потный вал вдохновенья"],
	[1817, "&quot;Золотой теленок&quot; XXIX. Гремящий Ключ"],
	[1823, "&quot;Золотой теленок&quot; XXX. Александр Ибн-Иванович"],
	[1828, "&quot;Золотой теленок&quot; XXXI. Багдад"],
	[1831, "&quot;Золотой теленок&quot; XXXII. Врата великих возможностей"],
	[1835, "&quot;Золотой теленок&quot; XXXIII. Индийский гость"],
	[1838, "&quot;Золотой теленок&quot; XXXIV. Дружба с юностью"],
	[1844, "&quot;Золотой теленок&quot; XXXV. Его любили домашние хозяйки, домашние работницы, вдовы и даже одна женщина - зубной техник"],
	[1850, "&quot;Золотой теленок&quot; XXXVI. Кавалер ордена Золотого Руна"],

	[1854,	"Полноте, ребята"],
	[1855,	"Светит месяц"],
	[1856,	"Заиграй, моя волынка"],
	[1857,	"Волга реченька, глубока"],
	[1858,	"Кумушки"],
	[1859,	"Ах ты, береза"],
	[1860,	"И.С. Бах Аллегро"],
	[1861,	"И.С. Бах. Скерцо"],
	[1862,	"В.В. Андреев. Мазурка"],
	[1863,	"Ах ты, вечер"],
	[1864,	"Из-за острова"],
	[9999,	""]
	];