this.map;
this.weatherLayer;
this.photoLayer;

this.poezd;
this.poezd_status = 0; //Состояние поезда: 0-стоит, 1-едет

this.path_times;
this.total_time = 0;
this.total_time_actual = 0;

this.total_dist = 0;
this.current_dist = 0;

this.current_vid = 0;
this.videos;
this.video_times;
this.video_times_actual;
this.video_lengths = [];

this.first_video = 'dgTFqyIDpQw';

this.is_loading = 1;
this.is_video_loaded = 0;
this.is_line_loaded = 0;
var polyline;

this.path_distances = [0];

this.current_time;
this.current_time_actual;
this.current_segment;

this.is_moov = 0;

this.elevator;
this.altitude = null;
this.lastUpdateAltitudeTime = 0;

const checkpoint_lat_cookie = 'checkpoint_lat';
const checkpoint_lng_cookie = 'checkpoint_lng';
const checkpoint_dist_cookie = 'checkpoint_dist';
const checkpoint_time_cookie = 'checkpoint_time';
const checkpoint_altitude = 'checkpoint_altitude';

const enableWeatherElementId = "enableWeather";
const enablePhotoElementId = "enablePhoto";

const quantum = 200;
const updateStatisticInterval = 1000;
const minUpdateAltitudeInterval = 15000; // ms

/**
* body onLoad
**/
function startPage() {
	wait();
	initMap();
	initVideo();
	setEventHandlers();
	jaaulde.utils.cookies.setOptions({expiresAt: Date.today().add(10).years()});
	refreshControls();
}

//==================================================================================

function getFirstVideo() {
	return this.first_video;
}

//==== VIDEO =======================================================================
function initVideo(/**yt_id, time**/) {
	var xmlhttp = getXmlHttp();
	rand = Math.random();
	/*
	if (rand < 0.3) {
		this.first_video = 'Dmc2STzMOT4';
	}
	else if (rand < 0.6) {
		this.first_video = 'v_f-JtAQuQE';
	}
	*/
	xmlhttp.open('GET', 'data/line_videos.txt', true);
	xmlhttp.onreadystatechange = function() {
	 	if (xmlhttp.readyState == 4) {
	 		if (xmlhttp.status == 200) {
				data = xmlhttp.responseText;
				setVideoTimes((data).split("\n"));

				swfobject.removeSWF("ytapiplayer");
				// <![CDATA[

				// allowScriptAccess must be set to allow the Javascript from one
				// domain to access the swf on the youtube domain
				var params = { allowScriptAccess: "always", bgcolor: "#cccccc" };
				// this sets the id of the object or embed tag to 'myytplayer'.
				// You then use this id to access the swf and make calls to the player's API
				var atts = { id: "myytplayer" };
				swfobject.embedSWF("http://www.youtube.com/v/" + getFirstVideo() + "&border=0&enablejsapi=1&playerapiid=ytplayer&autoplay=0&showsearch=0&rel=0",
				                 "ytapiplayer", "481", "289", "8", null, null, params, atts);
				//]]>
		 	}
			else {
				alert('Не найден список видеороликов.');
			}
	 	}
	};
	xmlhttp.send(null);
}

function onYouTubePlayerReady(playerId) {
	if (isLineLoaded()) {
		setHandlers();
		var yt_id = jaaulde.utils.cookies.get('yt_id');
		var seek = Number(jaaulde.utils.cookies.get('seek'));
		goToVideo(yt_id, seek, false);
	}
}

function setHandlers() {
	setClicks();

	startAudio();
	//goToVideo('dgTFqyIDpQw', 0, false);

	this.current_vid = 0;
	time = this.video_times[this.current_vid];
	this.current_segment = 0;
	from = getPathPoint(this.current_segment);
    poezd = createMarker(from);
    this.poezd = poezd;
	//setPoezd(poezd);
	this.poezd.setMap(this.map);

	ytplayer = document.getElementById("myytplayer");
	ytplayer.stopVideo();

	setInterval(updateytplayerInfo, quantum);
	updateytplayerInfo();

	setInterval(centerMap, updateStatisticInterval);

	setInterval(showStatistics, updateStatisticInterval);

	setListener();
	unwait();
}

function setClicks() {
var hilits = [
		[1, 'dgTFqyIDpQw', 0, 12],		//Москва
		//[2, '1Xdrbvv-O1c', 900],	//Петушки
		//[3, 'yEPpiZFn1YU', 675],	//Храм Покрова на Нерли
		//[4, 'NjkOQBPkbtQ', 0],		//р.Клязьма
		[5, 'u71nSGXoGic', 0, 12],		//р.Волга
		//[6, 'Sk3xK4Xck0I', 0],		//р.Вятка
		[7, 'mwjK6ZzURVM', 0, 12],		//р.Кама
		[8, '1OPbAaexHq4', 0, 12],		//Пермь
		[9, 'JQFBU5vwGvo', 2162, 12],	//Нижний Урал,с. Усть-Кишерть
		[10, 'k7_2BLIeBUU', 0, 13],		//Екатеринбург
		[11, 'cWSKL6-wmUo', 0, 13],		//Тюмень
		//[12, 's19S34_o-uc', 369],	//р.Тобол
		[13, 'Q1tWxA-lmak', 0, 12],		//р.Иртыш
		//[14, 'gpWrvWKe7f4', 0],		//Омск
		[15, 'yEPHRfxwSrQ', 1240, 12],	//Облака над Барабинской степью
		[16, 'I0VF78_viKM', 0, 12],		//р.Обь
		[17, 'FHhXzZ9NBxs', 0, 12],		//Новосибирск
		[18, '053FplLWBvQ', 252, 12],	//р.Кия
		[19, '_cvlkbN3xwQ', 0, 13],		//Красноярск
		[20, '_cvlkbN3xwQ', 300, 12],	//р.Енисей
		[21, 'sXuyVzWoEd0', 500, 12],	//с.Зыково, водонапорная башня в стиле модерн
		[22, 'iqu7hfp37Xk', 0, 12],		//р.Кан
		[23, 'zHBIQZbcVxg', 1680, 12],	//Тайшет
		//[24, 'dkXRoVvepWc', 0],		//р.Ока
		//[25, 'CWYahMQBsuc', 0],		//р.Иркут
		[26, 'BGey2L6jj90', 0, 13],		//Иркутск
		[27, 'BGey2L6jj90', 190, 12],	//р.Ангара
		[28, 'ecxKrJMEbWk', 1580, 12],	//Гладь оз.Байкал
		[29, 'ecxKrJMEbWk', 255, 14],	//Слюдянка
		//[30, 'NxDe-rhwB_s', 1310],	//оз. Байкал, п.Танхой
		[31, '_D44SFyHRrk', 0, 13],		//Улан-Удэ
		[32, 'f5F7kCAlMfI', 460, 12],	//Баргузинские горы
		[33, '0htOmH36yws', 260, 12],	//Петровский завод, г.Петровск-Забайкальский
		[34, 'AN9coQgjK7k', 1466, 12],	//оз.Кенон г.Чита
		[35, 'VFlxti1sK80', 420, 12],	//Хребет Цаган-Хуртей, п.Хилок
		[36, 'tzqL8ZJbAYo', 1150, 12],	//п.Карымское "Утро туманное"
		[37, 'fxD-HHU-WUE', 0, 12],		//Ерофей Павлович
		[38, 'svUTKAwnYIM', 0, 12],		//Рассвет над р.Зея
		[39, '2TJtQsCvojg', 0, 12],		//р.Томь
		[40, 'g5-B0_GtXU8', 425, 12],	//Бескрайняя Зейско-Буреинская равнина
		[41, '47lt9ULeKi4', 480, 12],	//Хинганский заповедник
		[42, '1Rkg3t6hmFI', 0, 14],		//Биробиджан
		[43, '8rUdbsffelE', 0, 12],		//р.Амур
		[44, 'Ij_ohbbePrE', 0, 14],		//Хабаровск
		[45, '23OUTclMsyY', 10, 12],	//Хехцирский хребет
		[46, 'Bd7dD8hzG1c', 0, 12],		//р.Бикин, пасмурный пейзаж
		[47, '9VaoM1mfg1A', 600, 14]	//Владивосток
	];
	hilits[0][1] = getFirstVideo();

	for (i=0; i<hilits.length; i++) {
		hlite = hilits[i];
		div_id = ['_goto_', hlite[0]];
		startBtn = document.getElementById(div_id.join(''));
		startBtn.onclick = function(yt_id, seek, zoom) {
			return function() {
				if (isLoading()) {
					return false;
				}
				goToVideo(yt_id, seek, false, zoom);
			}
		}(hlite[1], hlite[2], hlite[3]);
	}
}

function setListener() {
	ytplayer = document.getElementById("myytplayer");
	ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
	ytplayer.addEventListener("onError", "onPlayerError");
}

function saveCurrentPosition() {
	var currentTime = ytplayer.getCurrentTime();
	if ((Math.round(currentTime) % 5) == 0) {
		jaaulde.utils.cookies.set('yt_id', getVideoId(ytplayer));
		jaaulde.utils.cookies.set('seek', currentTime);
	}
}

function updateytplayerInfo() {
	if (!this.video_times) {
		return;
	}
	if (isLoading()) {
		return;
	}
	if (this.is_moov) {
		return;
	}
	ytplayer = document.getElementById("myytplayer");
	duration = ytplayer.getDuration();
	position = ytplayer.getCurrentTime();

	this.current_time = parseFloat(this.video_times[this.current_vid]) + parseFloat(position);
	this.current_time_actual = parseFloat(this.video_times_actual[this.current_vid]) + parseFloat(position);

	state = ytplayer.getPlayerState();
	if (state >= 0 && state < 3) {
		setMarkerPos(this.current_time);
	}

	//lbl = document.getElementById("time_label");
	//lbl_arr = [this.current_vid, this.videos[this.current_vid], this.current_segment];
	//lbl.innerHTML = lbl_arr.join(' : ');

	if (parseFloat(position) + 1 >= parseFloat(this.video_lengths[this.current_vid])) {
		playNextVideo();
	}
	saveCurrentPosition();
}

function centerMap() {
	if (isLoading()) {
		return;
	}
	if (this.is_moov) {
		return;
	}
	ytplayer = document.getElementById("myytplayer");
	ytplayer.mute();
	position = ytplayer.getCurrentTime();
	state = ytplayer.getPlayerState();
	if (this.poezd_status != 0 && state > 0 && state < 3 &&  position > 0) {
		to = this.poezd.getPosition();
		bounds = this.map.getBounds();
		if (!bounds.contains(to)) {
			this.map.setCenter(to);
		}
	}
	this.current_segment = getCurrentLineSegment(this.current_time);
}

function getCoordMinutes(coord) {
	return Math.floor((coord % 1) * 60);
}

function getCoordSeconds(coord) {
	return Math.floor((coord % (1 / 60)) * 3600)
}

function coordToStr(coord) {
	return Math.floor(coord) + '°' + getCoordMinutes(coord) + '′' + getCoordSeconds(coord) + '″';
}

function secondsToTimeStrWithDays(seconds) {
	var date = new Date(2000, 0, 1).addSeconds(seconds);
	var day = date.getDayOfYear();
	return day + "д " + date.toString("HH:mm:ss");    
}

function metersToKmStr(meters) {
	return (meters / 1000).toFixed(3);
}

function getSegmentLength(segment) {
	return path_distances[segment + 1] - path_distances[segment];
}

function getSegmentDuration(segment) {
	return path_times[segment + 1] - path_times[segment];
}

function getSegmentSpeedMps(segment) {
	return getSegmentLength(segment) / getSegmentDuration(segment);
}

function speedMpsToKmpsStr(speedMps) {
	return (speedMps * 3.6).toFixed(2);
}

function getAzimuthStr(from, to) {
	return Math.round(google.maps.geometry.spherical.computeHeading(from, to));
}

function getCheckpointFromDist() {
	return this.current_dist - parseFloat(jaaulde.utils.cookies.get(checkpoint_dist_cookie));
}

function getCheckpointFromTime() {
	return this.current_time_actual - parseFloat(jaaulde.utils.cookies.get(checkpoint_time_cookie));
}

function getCheckpointAvgSpeedMps() {
	var time = getCheckpointFromTime();
	if (time == 0)
		return 0;
	return getCheckpointFromDist() / time;
}

function getCheckpointAltitude() {
	return parseFloat(jaaulde.utils.cookies.get(checkpoint_altitude));
}

function showStatistics() {
	document.getElementById("_lat").innerHTML = coordToStr(this.poezd.getPosition().lat());
	document.getElementById("_lng").innerHTML = coordToStr(this.poezd.getPosition().lng());
	document.getElementById("_from_dist").innerHTML = metersToKmStr(this.current_dist);
	document.getElementById("_from_time").innerHTML = secondsToTimeStrWithDays(this.current_time_actual);    
	document.getElementById("_to_dist").innerHTML = metersToKmStr(this.total_dist - this.current_dist);
	document.getElementById("_to_time").innerHTML = secondsToTimeStrWithDays(this.total_time_actual - this.current_time_actual);
	document.getElementById("_progress_dist").innerHTML = (this.current_dist / this.total_dist * 100).toFixed(2);
	document.getElementById("_progress_time").innerHTML = (this.current_time_actual / this.total_time_actual * 100).toFixed(2);
	document.getElementById("_current_speed").innerHTML = speedMpsToKmpsStr(getSegmentSpeedMps(this.current_segment));
	document.getElementById("_avg_speed").innerHTML = speedMpsToKmpsStr(this.current_dist / this.current_time_actual);
	document.getElementById("_azimuth").innerHTML = getAzimuthStr(this.path_points[i],this.path_points[i + 1]);
	document.getElementById("_altitude").innerHTML = this.altitude.toFixed(1);
	document.getElementById("_frag_number").innerHTML = this.current_vid + 1;
	var checkPointActive = jaaulde.utils.cookies.get(checkpoint_dist_cookie);
	document.getElementById("checkpoint_lat").innerHTML = checkPointActive ? coordToStr(parseFloat(jaaulde.utils.cookies.get(checkpoint_lat_cookie))) : "";
	document.getElementById("checkpoint_lng").innerHTML = checkPointActive ? coordToStr(parseFloat(jaaulde.utils.cookies.get(checkpoint_lng_cookie))) : "";
	document.getElementById("checkpoint_from_dist").innerHTML = checkPointActive ? metersToKmStr(getCheckpointFromDist()) : "";
	document.getElementById("checkpoint_from_time").innerHTML = checkPointActive ? secondsToTimeStrWithDays(getCheckpointFromTime()) : "";
	document.getElementById("checkpoint_avg_speed").innerHTML = checkPointActive ? speedMpsToKmpsStr(getCheckpointAvgSpeedMps()) : "";
	document.getElementById("checkpoint_altitude").innerHTML = checkPointActive ? getCheckpointAltitude().toFixed(1) : "";
}

function onPlayerError(errorCode) {
	//stopMarker();
}

function onytplayerStateChange(newState) {
	var state = newState;
	if (newState == 5) {
		startMarker();
	}
	else if (newState == 1) {
		moveMarker();
	}
	else {
		stopMarker();
	}
}

function playNextVideo() {
	ytplayer = document.getElementById("myytplayer");
	this.current_vid++;
	video_id = this.videos[this.current_vid];
	ytplayer.cueVideoById(video_id, 0);
	ytplayer.playVideo();
}
//=======================================================
//==== AJAX =============================================
function getXmlHttp(){
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

//==== GOOGLE MAP =======================================
function getMap() {
	return this.map;
}

function initMap() {
 	this.map = new google.maps.Map(document.getElementById('map'), 
		{
			center: new google.maps.LatLng(55.75838, 37.66163),
			zoom: 12,
			mapTypeId: google.maps.MapTypeId.TERRAIN,
 			draggableCursor:'crosshair',
			disableDoubleClickZoom: true,
			scaleControl: false,
			streetViewControl: false,
			zoomControl: false,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
				mapTypeIds: [
					google.maps.MapTypeId.TERRAIN, 
					google.maps.MapTypeId.SATELLITE, 
					google.maps.MapTypeId.ROADMAP, 
					google.maps.MapTypeId.HYBRID
				]
			}
		}
	);

	this.elevator = new google.maps.ElevationService();
	google.maps.event.addListener(this.map, 'dblclick', 
		function (event) {
			getElevation(event);
		}
	);

	this.weatherLayer = new google.maps.weather.WeatherLayer({
		temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS
	});

	this.photoLayer = new google.maps.panoramio.PanoramioLayer();
	
	var xmlhttp = getXmlHttp()
	xmlhttp.open('GET', 'data/line_data.txt', true);

	xmlhttp.onreadystatechange = function() {
	 	if (xmlhttp.readyState == 4) {
	 		if (xmlhttp.status == 200) {
				data = xmlhttp.responseText;
				file_lines = (data).split("\n");
				pts = [];
				times = [];
				dists = [0];
				dist_from = 0;

				j = 0;
				for (i = 0; i < file_lines.length; i++) {
					// read each point on that line
					coords = (file_lines[i]).split("|");
					pts[i] = new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1]));
					times[i] = coords[2];
					if (i > 0) {
						dist_from = dist_from + google.maps.geometry.spherical.computeDistanceBetween(pts[i], pts[i-1]);
						dists[i] = Math.round(dist_from);
					}
				}

				setTotalDist(dists[dists.length - 1])
				setPathPoints(pts);
				setDistances(dists);
				setPathTimes(times);

				map = getMap();

				google.maps.event.addListener(map, "dragstart", function() {
					setMooving(1);
				});

				google.maps.event.addListener(map, "dragend", function() {
					setMooving(0);
				});

				loadOverlays(12);

				if (isVideoLoaded()) {
					setHandlers();
				}
			}
			else {
				alert('Не найдены данные для построения маршрута движения поезда.');
			}
	 	}
	};
	xmlhttp.send(null);
	initMapplet();
}

function setMooving(is_mooving) {
	this.is_moov = is_mooving;
}

function getZoomIdx(zoom) {
	if (zoom >= 12) {
		return 12;
	}
	return zoom;
}

function setMarkerPos(time) {
	segment = this.current_segment;

	if (segment < 0) {
		return;
	}
	from_vertex = getPathPoint(segment);
	to_vertex = getPathPoint(segment+1);
	part_time = time - getPathTime(segment);
	segment_time = getPathTime(segment + 1) - getPathTime(segment);
	var part = part_time/segment_time;

	steplat = (to_vertex.lat() - from_vertex.lat()) * part;
	steplng = (to_vertex.lng() - from_vertex.lng()) * part;

	lat = from_vertex.lat() + steplat;
	lng = from_vertex.lng() + steplng;
	to = new google.maps.LatLng(lat, lng);
	setPoezdPosition(to);
	this.current_dist = path_distances[segment] + (path_distances[segment + 1] - path_distances[segment]) * part;
}

function getCurrentLineSegment(time) {
	from = Math.round(time/63);
	if (this.path_times[from] <= time) {
		for (i=from; i<this.path_times.length; i++) {
			if (time < this.path_times[i]) {
				return i-1;
			}
		}
	}
	else {
		for (i=from; i>0; i--) {
			if (time > this.path_times[i]) {
				return i;
			}
		}
	}
	return -1;
}

function startMarker() {
	this.poezd_status = 0;
}

function stopMarker() {
	this.poezd_status = 0;
	pauseVideoAudio();
}

function moveMarker() {
	this.poezd_status = 1;
	playVideoAudio();
}

//==================================================================================
function loadOverlays() {
	var xmlhttp = getXmlHttp()
	xmlhttp.open('GET', 'data/line_enc_data.txt', false);
	xmlhttp.send(null);
	if (xmlhttp.status == 200) {
		var polyline = new google.maps.Polyline({
		  strokeColor: '#0000FF',
		  strokeWeight: 4,
		  strokeOpacity: 0.8,
		  path: google.maps.geometry.encoding.decodePath(xmlhttp.responseText),
		  geodesic: true
		});
		polyline.id = 1;
		google.maps.event.addListener(polyline, "click", function (event) {
			clickLine(event);
		});
		polyline.setMap(this.map);
	}
}

function createMarker(latlng) {
	var image = new google.maps.MarkerImage("img/poezd.png", 
		new google.maps.Size(39, 37), 
		new google.maps.Point(0, 0), 
		new google.maps.Point(19, 13)
	);
	var marker = new google.maps.Marker(
		{
			icon: image, 
			autoPan: true, 
			cursor: 'pinter',
			position: latlng 
		}
	);
	return marker;
}

function clickLine(event) {
	if (isLoading()) {
		return false;
	}
	wait();
	moveMarker();
	vertex = getNearestVertex(event.latLng);
	startPlay(vertex, false);

	from = getPathPoint(vertex);
	setPoezdPosition(from);
	unwait();
	showStatistics();
}

/**
* Определяет ближашую вершину ломаной
*/
function getNearestVertex(latlng) {
	vertex_num = 0;
	minDist = 60000000;
	delta = 60000000;
	for (i=0; i < getPathPointsLength(); i++) {
		vertex = getPathPoint(i);
		dist = google.maps.geometry.spherical.computeDistanceBetween(vertex, latlng);
		delta = dist;
		if (dist < minDist) {
			vertex_num = i;
			minDist = dist;
		}
		if (delta > dist) {
			break;
		}
	}
	return vertex_num;
}

/**
* Возвращает фрагмент видео, соответствующий вершине линии-пути
**/
function getVideoFragment(vertex) {
	time = getPathTime(vertex-1);
	for (i=1; i<this.videos.length; i++) {
		if (parseFloat(this.video_times[i]) > parseFloat(time)) {
			return i-1;
		}
	}
	return 0;
}

// ================================================

function setPoezd(poezd) {
	this.poezd = poezd;
}

function getPoezd() {
	return this.poezd;
}

function setPathTimes(path_times) {
	this.path_times	= path_times;
}

function getPathTime(vertex) {
	return this.path_times[vertex];
}

function setVideoTimes(varr) {
	var video_time_actual = 0;
	this.videos = new Array(varr.length);
	this.video_times = new Array(varr.length);
	this.video_times_actual = new Array(varr.length);
	var itms = ['', '', ''];
	for (i=0; i<varr.length; i++) {
		//Уродский фикс для IE. Не понятно, почму здесь не работает split, выше работает!
		itms[0] = (varr[i]).substr(0, 11);
		start_time = (varr[i]).substr(12, 7);
		itms[1] = parseInt(start_time.replace(/^\s+/, ""));
		itms[2] = parseInt((varr[i]).substr(20, 5));
		//item = (varr[i]).split('|');
		this.videos[i] = itms[0];
		this.video_times[i] = itms[1];
		this.video_lengths[i] = itms[2];
		this.video_times_actual[i] = video_time_actual;
		video_time_actual += itms[2];
	}
	this.videos[0] = getFirstVideo();
	this.total_time_actual = video_time_actual;
}

function setPathPoints(points) {
	this.path_points = points;
}

function setDistances(dists) {
	this.path_distances = dists;
}

function getPathPointsLength() {
	return this.path_points.length;
}

function getPathPoint(vertex) {
	return this.path_points[vertex];
}

function setTotalDist(value) {
	this.total_dist = value;
}

function wait() {
	mapdiv = document.getElementById('map');
	mapdiv.style.cursor = 'wait';
	this.is_loading = 1;
	waitdiv = document.getElementById('_wait');
	waitdiv.innerHTML = '<img src="img/loading.gif">';
}

function unwait() {
	mapdiv = document.getElementById('map');
	mapdiv.style.cursor = 'default';
	this.is_loading = 0;
	waitdiv = document.getElementById('_wait');
	waitdiv.innerHTML = '';
}

function isLoading() {
	return this.is_loading > 0;
}

function setCurrentVid(vid) {
	this.current_vid = vid;
}

function startPlay(vertex, start) {
	vid = getVideoFragment(vertex);
	seek = getPathTime(vertex) - this.video_times[vid];
	if (seek <= 0) {
		seek = 0;
	}
	startPlayVideo(vid, seek, start);
}

function startPlayVideo(vid, seek, start) {
	setCurrentVid(vid);
	ytplayer = document.getElementById("myytplayer");
	if (ytplayer.getPlayerState() >= 0) {
		ytplayer.stopVideo();
		//ytplayer.clearVideo();
	}

	vid_id = getVideoId(ytplayer);
	if (vid_id != this.videos[vid]) {
		ytplayer.cueVideoById(this.videos[vid], seek);
		ytplayer.seekTo(seek, true);
	}
	else {
		ytplayer.seekTo(seek, true);
	}
	if (start) {
		ytplayer.playVideo();
		startMarker();
	}
	else {
		ytplayer.pauseVideo();
	}
}

function getVideoId(ytplayer) {
	var reg = /[\?&]v=([^&\s]+)/i;
	arr = reg.exec(ytplayer.getVideoUrl());
	if (arr != null) {
		return arr[1];
	}
	return false;
}

// ================================================
function goToVideo(yt_id, seek, start, zoom) {
	wait();
	for (i=0; i<this.videos.length; i++) {
		if (this.videos[i] == yt_id) {
			this.current_vid = i;
			break;
		}
	}
	time = this.video_times[this.current_vid] + seek;
	for (i=0; i<this.path_times.length; i++) {
		if (this.path_times[i] > time) {
			this.current_segment = i-1;
			break;
		}
	}
	old_poezd = getPoezd();
	from = getPathPoint(this.current_segment);

	setPoezdPosition(from);
	this.map.setCenter(poezd.getPosition(), zoom);

	startPlayVideo(this.current_vid, seek, start);
	unwait();
	showStatistics();
}

function isVideoLoaded() {
	if (this.is_video_loaded > 0)
		return true;
	else
		this.is_line_loaded = 1;
}

function isLineLoaded() {
	if (this.is_line_loaded > 0)
		return true;
	else
		this.is_video_loaded = 1;
}

function refreshControls() {
	refreshEnableWeather();
	refreshEnablePhoto();
}

function setEventHandlers() {
	document.getElementById("setCheckpoint").onclick = 
		function ()	{
			setCheckpoint();
		};
	document.getElementById("deleteCheckpoint").onclick = 
		function ()	{
			deleteCheckpoint();
		};
	document.getElementById(enableWeatherElementId).onchange = 
		function ()	{
			refreshEnableWeather();
		};
	document.getElementById(enablePhotoElementId).onchange = 
		function ()	{
			refreshEnablePhoto();
		};
}

function setCheckpoint() {
	var latLng = window.poezd.getPosition();
	jaaulde.utils.cookies.set(checkpoint_lat_cookie, latLng.lat());
	jaaulde.utils.cookies.set(checkpoint_lng_cookie, latLng.lng());
	jaaulde.utils.cookies.set(checkpoint_dist_cookie, this.current_dist);
	jaaulde.utils.cookies.set(checkpoint_time_cookie, this.current_time_actual);
	jaaulde.utils.cookies.set(checkpoint_altitude, this.altitude);
}

function deleteCheckpoint() {
	jaaulde.utils.cookies.del(checkpoint_lat_cookie);
	jaaulde.utils.cookies.del(checkpoint_lng_cookie);
	jaaulde.utils.cookies.del(checkpoint_dist_cookie);
	jaaulde.utils.cookies.del(checkpoint_time_cookie);
	jaaulde.utils.cookies.del(checkpoint_altitude);
}

function setPoezdPosition(latLng) {
	this.poezd.setPosition(latLng);
	updateAltitude(latLng);
}

function updateAltitude(latlng) {
	var currentTime = new Date().getTime();
	if (currentTime - this.lastUpdateAltitudeTime < minUpdateAltitudeInterval) 
		return;
	this.lastUpdateAltitudeTime = currentTime;
	var locations = [];
	locations.push(latlng);
	var positionalRequest = {
		locations: locations
	}
	this.elevator.getElevationForLocations(positionalRequest, 
		function (results, status) {
			if ((status == google.maps.ElevationStatus.OK) && (results[0])) {
				this.altitude = results[0].elevation;
			} else {
				this.altitude = null;
			}
		}
	);
}

function refreshEnableWeather() {
	var enableWeatherElement = document.getElementById(enableWeatherElementId);
	if (enableWeatherElement.checked) {
		this.weatherLayer.setMap(map);
	}
	else {
		this.weatherLayer.setMap(null);
	}
}

function refreshEnablePhoto() {
	var enablePhotoElement = document.getElementById(enablePhotoElementId);
	if (enablePhotoElement.checked) {
		this.photoLayer.setMap(map);
	}
	else {
		this.photoLayer.setMap(null);
	}
}
