this.uppod_ready = 0;
this.audio_channel = "-10";
//this.poezd_status = 1;
this.player_status = 0;

function radioOn() {
	//document.getElementById('audioplayer1').innerHTML = '';
	var flashvars = {"uid":"audioplayer1","st":"uppod/rstyle.txt","file":"http://217.10.44.211:8000/russianradio128.mp3","comment":"Русское радио"};
	var params = {bgcolor:"#ffffff", allowFullScreen:"false",allowScriptAccess:"always",wmode:"window"};
	var attributes={id:"audioplayer1"};
	new swfobject.embedSWF("uppod/uppod.swf", "audioplayer1", "210", "20", "9.0.0", false, flashvars, params,attributes);
	this.audio_channel = 0;
	setBookName();
}

function bookOn(item) {
	if (this.audio_channel < 1) {
		//document.getElementById('audioplayer1').innerHTML = '';
		var flashvars = {"uid":"audioplayer1","st":"uppod/abstyle.txt","pl":"61AchBGD7xYBTckYf42MYipsNy9m06VT2UhTd07nZWwbHsGr9","plstart":item};
		var params = {bgcolor:"#ffffff", allowFullScreen:"false",allowScriptAccess:"always",wmode:"window"};
		var attributes={id:"audioplayer1"};
		new swfobject.embedSWF("uppod/uppod.swf", "audioplayer1", "210", "20", "9.0.0", false, flashvars, params,attributes);
	}
	else {
		uppodSend('audioplayer1', 'start' + item, 'void');
	}
	this.audio_channel = item;
	setBookName();
}

function videoAudioOn() {
	//document.getElementById('audioplayer1').innerHTML = '';
	var flashvars = {"uid":"audioplayer1","st":"uppod/vstyle.txt","file":"data/train.mp3","comment":"Стук колес"};
	var params = {bgcolor:"#ffffff", allowFullScreen:"false",allowScriptAccess:"always",wmode:"window"};
	var attributes={id:"audioplayer1"};
	new swfobject.embedSWF("uppod/uppod.swf", "audioplayer1", "210", "20", "9.0.0", false, flashvars, params, attributes);
	this.audio_channel = '-1';
	setBookName();
}

function setBookName() {
	if (parseInt(this.audio_channel) == -1) {
		document.getElementById('_audio_name').innerHTML = 'Стук колес';
		return;
	}
	if (this.audio_channel == 0) {
		document.getElementById('_audio_name').innerHTML = '<a href="http://rusradio.ru" target="_blank">Русское радио</a>&nbsp;<img src="img/external.png">';
		return;
	}
	audiolabel = "&quot;Война и мир&quot; том 1, ч.1, гл.1";
	for (i=1; i < playlist_names.length; i++) {
		if (this.audio_channel < playlist_names[i][0]) {
			if (this.audio_channel >= 1854) {
				document.getElementById('_audio_name').innerHTML = '<a href="http://music.lib.ru/s/shezhin_w_g/" target="_blank">' + audiolabel + '</a>&nbsp;<img src="img/external.png">';
			}
			else {
				document.getElementById('_audio_name').innerHTML = '<a href="http://ardisbook.ru" target="_blank">' + audiolabel + '</a>&nbsp;<img src="img/external.png">';
			}
			return;
		}
		audiolabel = playlist_names[i][1];
	}
	document.getElementById('_audio_name').innerHTML = "";
}

function switchAudio(chan) {
	if (!this.uppod_ready) {
		return false;
	}
	this.uppod_ready = 0;

	if (getMetachannel(chan) != getMetachannel(this.audio_channel)) {
		sel2 = document.getElementById('_audio_sel_2');
		sel2.disabled = true;
		sel2.options.length = 0;
		/**
		for (j = sel2.options.length - 1; j >= 0; j--) {
			sel2.remove(i);
		}
		**/
		if (chan > 0) {
			sel2.disabled = false;
			idx = getMetachannel(chan);
			for (i = 0; i < book_items[idx].length; i++) {
				book_item = book_items[idx][i];
				addOption(sel2, book_item[1], book_item[0], i == 0);
			}
		}
	}

	if (chan == '-1') { //Стук колес
		videoAudioOn();
	}
	else if (chan == 0) { //Радио
		radioOn();
	}
	else if (chan > 0) { //Аудиокнига
		bookOn(chan);
	}

	this.uppod_ready = 1;
}

function getMetachannel(item) {
	if (item < 0) {
		return -1;
	}
	if (item == 0) {
		return 0;
	}
	if (item < 1474) {
		return 2;
	}
	if (item < 1656) {
		return 3;
	}
	if (item < 1854) {
		return 4;
	}
	return 1;
}

function addOption (oListbox, text, value, isSelected) {
	var oOption = document.createElement("option");
	oOption.appendChild(document.createTextNode(text));
	oOption.setAttribute("value", value);
	if (isSelected) {
		oOption.selected = true;
	}
	oListbox.appendChild(oOption);
}

function sitchAudio2(chan) {
	bookOn(chan);
}

function startAudio() {
	sel = document.getElementById('_audio_sel');
	this.uppod_ready = 1;
 	switchAudio(sel.value);
}

function uppodInit() {
	timer_id = setTimeout( function() {
		if (this.audio_channel == '-1') {
			if (this.poezd_status == 0) {
				pauseVideoAudio();
			}
			else {
				playVideoAudio();
			}
		}
		else {
			uppodSend('audioplayer1', 'play', 'void');
		}
	}, 1000);
	clearTimeout(timer_id);
	if (parseInt(this.audio_channel) == 0) {
		uppodSend('audioplayer1', 'play', 'void');
	}
	//setTimeout(1000, 'onSongChange');
}

function uppodStartsReport(playerID) {
	uppodGetStatus(this.player_status);
	uppodSend('audioplayer1', 'getstatus', 'uppodGetStatus');
	if (this.audio_channel == '-1') {
		if (this.poezd_status == 0) {
			pauseVideoAudio();
		}
		else {
			playVideoAudio();
		}
	}
	else {
		uppodSend('audioplayer1', 'play', 'void');
	}
	if (this.audio_channel > 0) {
		uppodSend('audioplayer1', 'getpl', 'onStartPlay');
	}
}

function onStartPlay(item) {
	if (item != this.audio_channel && item > 0) {
		setChannel(item);
	}
}

function uppodGetStatus(status) {
	if (status > 0) {
		document.getElementById('_play_btn').src = 'img/pause_btn.gif';
	}
	else {
		document.getElementById('_play_btn').src = 'img/play_btn.gif';
	}
}

function pauseVideoAudio() {
	if (this.audio_channel == '-1') {
		uppodSend('audioplayer1', 'pause', 'void');
	}
}

function playVideoAudio() {
	if (this.audio_channel == '-1') {
		uppodSend('audioplayer1', 'play', 'void');
	}
}

function setChannel(item) {
	this.uppod_ready = 0;
	sel = document.getElementById('_audio_sel');
	metasel = getMetachannel(item);
	for (i=0; i < 6; i++) {
	    if (metasel == (i-1)) {
	        sel.options[i].selected = true;
	    }
	    else {
	        sel.options[i].selected = false;
	    }
	}

	sel = document.getElementById('_audio_sel_2');
	if (item != sel.value) {
		for (i=0; i < sel.options.length; i++) {
		    if ((sel.options[i].value <= item) && ((i == (sel.options.length - 1)) || (item < sel.options[i+1].value))) {
		        sel.options[i].selected = true;
		    }
		    else {
		        sel.options[i].selected = false;
		    }
		}
		this.audio_channel = item;
	}
	this.uppod_ready = 1;
	setBookName();
}


function playBtnBw() {
	if (!this.uppod_ready) {
		return false;
	}
	if (this.audio_channel == "-1") {
		return false;
	}
	else if (this.audio_channel == 1854) {
		nval = 0;
	}
	else if (this.audio_channel == 1) {
		nval = 1864;
	}
	else {
		nval = parseInt(this.audio_channel) - 1
	}
	switchAudio(nval);
	setChannel(this.audio_channel);
}

function playBtnClick() {
	if (!this.uppod_ready) {
		return false;
	}
	if ((parseInt(this.audio_channel) == -1) && (this.poezd_status == 0)) {
		return false;
	}
	uppodSend('audioplayer1', 'toggle', 'void');
	uppodSend('audioplayer1', 'repeat');
}

function playBtnFw() {
	if (!this.uppod_ready) {
		return false;
	}
	if (this.audio_channel == 1853) {
		return false;
	}
	else if (this.audio_channel == 0) {
		nval = 1854;
	}
	else if (this.audio_channel == 1864) {
		nval = 1;
	}
	else {
		nval = parseInt(this.audio_channel) + 1
	}
	switchAudio(nval);
	setChannel(this.audio_channel);
}